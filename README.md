### Visualisation, analyse des données

0. Statistiques de base
1. Voir utilisateur avec le log le plus long
2. Entropie, stationarité, régularité
4. Heatmap
5. Mixtures de gaussiennes

### Traitement

3. Interpolation

### Modélisation, prédiction

6. HMM
7. Modèles plus avancés (articles)
8. Réseaux de neurones


### À régler
enregistrement des données intermédiaires
* ndarray positions


multiplot
