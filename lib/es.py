import datetime
import os
import numpy as np

def ouvrir_plt(chemin):
    with open(chemin) as file:
        latitudes = []
        longitudes = []
        altitudes = []
        dates = []
        for line in file.readlines()[6:]:
            lat, lon, _, alt, _, date, time = line.strip().split(',')
            dttm = datetime.datetime.strptime(date+' '+time, "%Y-%m-%d %H:%M:%S")
            latitudes.append(float(lat))
            longitudes.append(float(lon))
            altitudes.append(float(alt))
            dates.append(dttm)
    return [latitudes, longitudes, altitudes, dates]

def open_plt(path):
    with open(path) as file:
        points = []
        for line in file.readlines()[6:]:
            lat, lon, _, alt, _, date, time = line.strip().split(',')
            dttm = datetime.datetime.strptime(date+' '+time, "%Y-%m-%d %H:%M:%S")
            
            points.append([float(lat), float(lon), float(alt), dttm])
    return points

def abrir_plt(path):
    points = np.loadtxt(path, delimiter=',', skiprows=6, usecols=(0,1))
    dates = np.loadtxt(path, dtype='str', delimiter=',', skiprows=6, usecols=(5,6))
    return points, dates

def ouvrir_utilisateur(chemin):
    coordonnees = [[], [], [], []]
    for filename in os.listdir(chemin):
        plt = ouvrir_plt(chemin+'/'+filename)
        for i in range(4):
            coordonnees[i] += plt[i]
    return coordonnees

def open_user(path):
    points = []
    for filename in os.listdir(path):
        plt = open_plt(path+'/'+filename)
        points.extend(plt)
    return points

def abrir_usuario(path):
    points = np.empty(shape=[0, 2])
    dates = np.empty(shape=[0, 2])
    for filename in os.listdir(path):
        points_i, dates_i = abrir_plt(path+'/'+filename)
        points = np.concatenate((points, points_i))
        dates = np.concatenate((dates, dates_i))
    return points, dates

def abrir_usuario_2(path):
    rows = 0
    for filename in os.listdir(path):
        with open(filename, 'r') as f:
            for line in f:
                rows += 1
        rows -= 6
        
    points = np.empty((rows, 2))
    dates = np.empty((rows, 2), dtype='str')
    
    #for filename in os.listdir(path):
    return points, dates

def TraceByUser(path):
    #concatene toutes les traces (tous les fichiers) de l'uitlisateur en parametre
    UserL = np.array([])
    cpt = 0
    for i in os.listdir(path):
        n = np.loadtxt(path+'/'+i, delimiter=",", skiprows=6 ,dtype='str')
        if(cpt==0):
            UserL = n
            cpt = 1
        else:
            UserL = np.vstack((UserL,n))
    return UserL

def ouverture_ultime(path):
    pos = []
    dates = []
    for filename in sorted(os.listdir(path)):
        with open(path+'/'+filename, 'r') as f:
            for line in f.readlines()[6:]:
                lat, lon, _, alt, _, date, time = line.strip().split(',')
                #dttm = datetime.datetime.strptime(date+' '+time, "%Y-%m-%d %H:%M:%S")
                dttm = np.datetime64(date+'T'+time)
                pos.append([np.float(lat), np.float(lon)])
                dates.append(dttm)
    return np.array(pos), np.array(dates)
    
def ouvrir_utilisateurs(path):
    pos = {}
    dates = {}
    for foldername in sorted(os.listdir(path)):
        pos[foldername], dates[foldername] = ouverture_ultime(path+'/'+foldername+'/Trajectory')
    return pos, dates


#Tester ouverture de l'utilisateur 153 (secondes)
#* Ouvrir_utilisateur : 22.4
#* Open_user : 23.4
#* Abrir_usuario : 257 _causé par le np.concatenate qui recopie les 2 arrays_
#* TraceByUser : 545 _idem_
#* Ouverture ultime : 7.4


