import numpy as np

def histogramme(coordonnees, nombre_sections):
    xmin = min(coordonnees[:, 1])
    xmax = max(coordonnees[:, 1])
    ymin = min(coordonnees[:, 0])
    ymax = max(coordonnees[:, 0])
    densite = np.zeros([nombre_sections, nombre_sections], dtype=int)
    pas_lat = (ymax - ymin)/nombre_sections
    pas_lon = (xmax - xmin)/nombre_sections
    for point in coordonnees:
        densite[min(nombre_sections-1, int((ymax - point[0])//pas_lat)), min(nombre_sections-1, int((point[1] - xmin)//pas_lon))] += 1
    densite = densite / len(coordonnees)
    return densite


